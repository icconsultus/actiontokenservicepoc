const
  mongoClient = require('mongodb').MongoClient,
  request = require('request'),
  RSVP = require('rsvp'),
  uuidv4 = require('uuid/v4')

const
  ActionTokenAPI = {};

(function(){
  ActionTokenAPI.totpServiceURL = ""
  ActionTokenAPI.totpServiceAPIKey = ""

  ActionTokenAPI.actionTokenCollection = {}

  ActionTokenAPI.mongoDB_URL = ""
  ActionTokenAPI.mongoDB_Collection = ""

  // Init function
  ActionTokenAPI.initialize = initializeActionTokenAPI

  // Implementation of the various routes
  ActionTokenAPI.post = doActionToken_POST
  ActionTokenAPI.get  = doActionToken_GET
  ActionTokenAPI.head  = doActionToken_HEAD
  ActionTokenAPI.delete  = doActionToken_DELETE
})()

module.exports = ActionTokenAPI


function initializeActionTokenAPI(mongoDB_URL, mongoDB_Collection, totpServiceURL, totpServiceAPIKey){
  console.log( 'initializeActionTokenAPI()')
  console.log( '  mongoDB_URL=' + mongoDB_URL)
  console.log( '  mongoDB_Collection=' + mongoDB_Collection)
  console.log( '  totpServiceURL=' + totpServiceURL)
  console.log( '  totpServiceAPIKey=' + totpServiceAPIKey)

  ActionTokenAPI.mongoDB_URL = mongoDB_URL
  ActionTokenAPI.mongoDB_Collection = mongoDB_Collection
  ActionTokenAPI.totpServiceURL = totpServiceURL
  ActionTokenAPI.totpServiceAPIKey = totpServiceAPIKey

  mongoClient.connect(mongoDB_URL, function(err, db) {
    console.log( 'initializeActionTokenAPI(): mongoClient connected' )

    if(err){
      console.log(err)
      process.exit(-1)
    } else {
      db.createCollection(mongoDB_Collection, function(err, collection) {
        console.log( 'initializeActionTokenAPI(): collection created/loaded: ' + mongoDB_Collection)
        ActionTokenAPI.actionTokenCollection = collection
      })
    }
  })
}

function doActionToken_HEAD(req, res){
  console.log('HEAD /ActionToken/:id')
  console.log( ' Headers: ' + JSON.stringify(req.headers))
  console.log( ' Parameters: ' + JSON.stringify(req.params))
  console.log( ' Body: ' + JSON.stringify(req.body))

  var actionTokenID = req.params.id


  ActionTokenAPI.actionTokenCollection.findOne({'_id': actionTokenID}, function(err, item) {
    if(err){
      res.status(500)
    } else {
      console.log( ' actionToken=' + JSON.stringify(item) )

      if(item === null){
        res.status(404)
      } else {
        if(item.status === 'auth_pending'){
          res.status(404)
        } else if(item.status === 'auth_failed'){
          res.status(404)
        } else {
          res.status(200)
        }
      }
    }
  })
}

function doActionToken_DELETE(req, res){
  console.log('DELETE /ActionToken/:id')
  console.log( ' Headers: ' + JSON.stringify(req.headers))
  console.log( ' Parameters: ' + JSON.stringify(req.params))
  console.log( ' Body: ' + JSON.stringify(req.body))

  var actionTokenID = req.params.id

  ActionTokenAPI.actionTokenCollection.remove({'_id': actionTokenID})

  res.status(200).send('Ok')
}

function doActionToken_GET(req, res){
  // verifyRequestSignature(req)

  console.log('GET /ActionToken/:id')
  console.log( ' Headers: ' + JSON.stringify(req.headers))
  console.log( ' Parameters: ' + JSON.stringify(req.params))
  console.log( ' Body: ' + JSON.stringify(req.body))

  var actionTokenID = req.params.id

  ActionTokenAPI.actionTokenCollection.findOne({'_id': actionTokenID}, function(err, item) {
    if(err){
      res.status(500).send("Unkown error")
    } else {
      console.log( ' actionToken=' + JSON.stringify(item) )

      if(item === null){
        res.status(404).send('ActionTokenID not found')
      } else {
        if(item.status === 'auth_pending'){
          res.status(404).send('Authentication pending')
        } else if(item.status === 'auth_failed'){
          res.status(404).send('Authentication failed')
        } else {
          res.status(200).send(item.actionToken)
        }
      }
    }
  })
}

/*
 *  Flow:
 *    - receive ActionTokenRequest through POST and process/verify Parameters
 *    - create an identifier, store ActionTokenRequest in DB among identifier
 *    - verify if the requested scopes are legit through checking the policies defined by the user
 *    - trigger request for MFA through looking up of information in user profile, then send through PushNotification
  *    - send back HTTP response with identifier to enable pushing of ActionToken, once MFA transaction is completed
 */
function doActionToken_POST(req, res){
  // verifyRequestSignature(req)

  console.log('POST /ActionToken')
  console.log( ' Headers: ' + JSON.stringify(req.headers))
  console.log( ' Parameters: ' + JSON.stringify(req.params))
  console.log( ' Body: ' + JSON.stringify(req.body))

  verifyActionTokenRequest(req.body).then(
    function(result){
      // create a new Actiontoken ID
      var actionTokenID = uuidv4()

      console.log( 'doActionToken_POST()')
      console.log( '  result=' + JSON.stringify(result))
      console.log( '  actionTokenID=' + actionTokenID)

      // store the ActionTokenRequest for further processing
      // set the state to 'auth_pending'
      ActionTokenAPI.actionTokenCollection.insertOne({
        '_id': actionTokenID,
        'status':'auth_pending',
        'actionTokenRequest': result
      })

      // trigger the MFA process
      startAuthentication({sub: result.sub, aud: result.aud, actionTokenID: actionTokenID})

      // return with a 202, provide the actionTokenID as the return parameter
      res.status(202).send({'id':actionTokenID})
    }).catch(function(error){
      res.status(error.code).send(error.message)
    })
}



function startAuthentication(params){
  var subject = params.sub
  var audience = params.aud
  var actionTokenID = params.actionTokenID

  console.log( 'startAuthentication()' )
  console.log( '  subject=' + subject )
  console.log( '  audience=' + audience )
  console.log( '  actionTokenID=' + actionTokenID )
  console.log( '  totpServiceUR=' + ActionTokenAPI.totpServiceURL )
  console.log( '  totpServiceAPIKey=' + ActionTokenAPI.totpServiceAPIKey )

  request.post({
    url: ActionTokenAPI.totpServiceURL+"/startAuthentication",
    headers: {
      "apikey":ActionTokenAPI.totpServiceAPIKey,
      "Content-Type":"application/json",
      "Accept":"application/json"
    },
    json:{
      "sub": subject,
      "aud": audience,
      "totpRequestID": actionTokenID
    }
  }, function(err, response, body){
    if(err){
      console.log(err)
      throw new Error('error starting authentication')
    } else {
      console.log( 'startAuthentication(): successfully started')
    }
  })
}

/*
 * - checks if all required parameters are provided
 * - checks if requested scopes are allowed in user profile
 */
function verifyActionTokenRequest(requestBody){
  return new RSVP.Promise(function(resolve, reject){
    console.log( 'verifyPostParameters(): requestBody=' + JSON.stringify(requestBody))

    if(requestBody === undefined){
      reject({code: 400, message: "no parameters in POST request provided"})
    }

    if(requestBody.sub === undefined){
      reject({code: 400, message: "sub parameter in POST request not provided"})
    }

    if(requestBody.aud === undefined){
      reject({code: 400, message: "aud parameter in POST request not provided"})
    }

    if(requestBody.dai_az_scope === undefined){
      reject({code: 400, message: "dai_az_scope parameter in POST request not provided"})
    }

    /*
     * parameters are ok, now validate if request scopes are legit
     *
     *  - verify subject  check if MFA is set up
     *  - verify audience
     *  - verify scopes
     *
     */

    console.log( 'verifyPostParameters(): parameters ok')
    resolve({sub: requestBody.sub, aud: requestBody.aud, dai_az_scope: requestBody.dai_az_scope})
  })
}
