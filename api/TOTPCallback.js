const
  mongoClient = require('mongodb').MongoClient,
  request = require('request'),
  RSVP = require('rsvp'),
  uuidv4 = require('uuid/v4'),
  jwt =require('jsonwebtoken')

const
  TOTPCallbackAPI = {};

(function(){
  TOTPCallbackAPI.actionTokenCollection = {}

  TOTPCallbackAPI.mongoDB_URL = ""
  TOTPCallbackAPI.mongoDB_Collection = ""

  // Init function
  TOTPCallbackAPI.initialize = initializeTOTPCallbackAPI

  // Implementation of the various routes
  TOTPCallbackAPI.post = doTOTPCallback_POST
})();

module.exports = TOTPCallbackAPI


function initializeTOTPCallbackAPI(mongoDB_URL, mongoDB_Collection){
  console.log( 'initializeTOTPCallbackAPI()');
  console.log( '  mongoDB_URL=' + mongoDB_URL);
  console.log( '  mongoDB_Collection=' + mongoDB_Collection);

  TOTPCallbackAPI.mongoDB_URL = mongoDB_URL
  TOTPCallbackAPI.mongoDB_Collection = mongoDB_Collection

  mongoClient.connect(mongoDB_URL, function(err, db) {
    console.log( 'initializeTOTPCallbackAPI(): mongoClient connected' );

    if(err){
      console.log(err);
      process.exit(-1);
    } else {
      db.createCollection(mongoDB_Collection, function(err, collection) {
        console.log( 'initializeTOTPCallbackAPI: collection created/loaded: ' + mongoDB_Collection);
        TOTPCallbackAPI.actionTokenCollection = collection;
      });
    }
  });

}

function doTOTPCallback_POST(req, res){
  console.log('doTOTPCallback_POST()');
  console.log( ' Headers: ' + JSON.stringify(req.headers));
  console.log( ' Parameters: ' + JSON.stringify(req.params));
  console.log( ' Body: ' + JSON.stringify(req.body));

  var actionTokenID = req.body.totpRequestID;
  var authenticationStatus = req.body.authenticationStatus;
  var tokenStatus = 'auth_failed';
  var claims = {};
  var actionToken = {};

  if(actionTokenID === undefined){
    res.status(403).send('actionTokenID is missing');
  } else if(authenticationStatus === undefined){
    res.status(403).send('authenticationStatus is missing');
  } else {
    tokenStatus = (authenticationStatus==='auth_success')?'created':'auth_failed';
    console.log( 'doTOTPCallback_POST(): will use tokenStatus=' + tokenStatus );

    // lookup the ActionToken data we have so we can update the status
    TOTPCallbackAPI.actionTokenCollection.findOne({'_id': actionTokenID}, function(err, item) {
      if(err){
        console.log( 'doTOTPCallback_POST(): error looking up item');
        res.status(400).send('error looking up item')
      } else {
        if(!item){
          console.log( 'doTOTPCallback_POST(): item not found');
          res.status(400).send('item not found')
        } else {
          if(authenticationStatus ==='auth_success'){
            claims = buildClaims(item.actionTokenRequest.sub, item.actionTokenRequest.aud, actionTokenID);
            actionToken = jwt.sign(claims, 'sharedsecret');

            console.log( 'will insert into DB token=' + actionToken );

            TOTPCallbackAPI.actionTokenCollection.update(
              {'_id':actionTokenID},
              {$set:{'status':tokenStatus, 'actionToken':actionToken }},
              {upsert: true} );

            res.status(200).send('');
          } else {
            actionToken = {};

            console.log( 'will insert into DB token=' + actionToken );

            TOTPCallbackAPI.actionTokenCollection.update(
              {'_id':actionTokenID},
              {$set:{'status':tokenStatus, 'actionToken':actionToken }},
              {upsert: true} );

            res.status(200).send('');
          }
        }
      }
    });
  }
}

function buildClaims(sub, aud, jti){
  return {
    "iss": "Daimler Action Token Service ID",
    "sub": sub,
    "aud": aud,
    "exp": "1481259600",
    "iat": "1481259100",
    "jti": jti,
    "scope": {
      "vin": "WVGLG77L76D021927",
      "urls": [
        {
          "u": "https://api.mercedes.com/v1/lights"
        },
        {
          "m": "POST",
          "u": "https://api.mercedes.com/v1/door"
        },
        {
          "m": "GET",
          "u": "https://api.mercedes.com/v1/engine"
        }]}}}
