// https://arcane-plateau-34250.herokuapp.com/
  
const
  bodyParser = require('body-parser'),
  express = require('express'),
  xhub = require('express-x-hub'),
  RSVP = require('rsvp'),
  config = require('config'),
  crypto = require('crypto')

const
  TOTPCallbackAPI = require('./api/TOTPCallback.js'),
  ActionTokenAPI = require('./api/ActionToken.js')

const
  app = express()

// Read in configuration
// App Secret can be retrieved from the App Dashboard
const HMAC_SECRET = (process.env.HMAC_SECRET) ?
  process.env.HMAC_SECRET :
  config.get('hmacSecret')

const MONGODB_URL = (process.env.MONGODB_URL) ?
  process.env.MONGODB_URL :
  config.get('mongodbURL')

const DB_ACTIONTOKENS = (process.env.DB_ACTIONTOKENS) ?
  process.env.DB_ACTIONTOKENS :
  config.get('dbActiontokens')

const TOTP_SERVICE_URL = (process.env.TOTP_SERVICE_URL) ?
  process.env.TOTP_SERVICE_URL :
  config.get('totpServiceURL')

const TOTP_SERVICE_API_KEY = (process.env.TOTP_SERVICE_API_KEY) ?
  process.env.TOTP_SERVICE_API_KEY :
  config.get('totpServiceAPIKey')
// --- End Read in configuration

// initialize the various API endpoint implementations
ActionTokenAPI.initialize(
  MONGODB_URL,
  DB_ACTIONTOKENS,
  TOTP_SERVICE_URL,
  TOTP_SERVICE_API_KEY)

TOTPCallbackAPI.initialize(
  MONGODB_URL,
  DB_ACTIONTOKENS)

// Express setup
app.set('port', process.env.PORT || 5000)
// app.use(xhub({ algorithm: 'sha1', secret: HMAC_SECRET }))
app.use(bodyParser.json({
  type: function() {
        return true
    }
}))

app.listen(app.get('port'), function() {
  console.log('up and running on port', app.get('port'))
})
// --- End Setup Express

/*
 * Express Routes
 */

app.post('/TOTPCallback', TOTPCallbackAPI.post)
app.post('/ActionToken', ActionTokenAPI.post)
app.get('/ActionToken/:id', ActionTokenAPI.get)
app.head('/ActionToken/:id',ActionTokenAPI.head)
app.delete('/ActionToken/:id',ActionTokenAPI.delete)

function verifyRequestSignature(req){
  if (req.isXHub) {
    console.log('+ request header X-Hub-Signature found, validating')
    if (req.isXHubValid()) {
      console.log('++ request header X-Hub-Signature validated')
    } else {
      console.log('!! request header X-Hub-Signature not validated')
      // return this.reject('x-hub-signature invalid', req, res)
      throw new Error("x-hub-signature invalid")
    }
  } else {
    console.log('!! request header X-Hub-Signature not found')
    // return this.reject('no x-hub-signature ', req, res)
    throw new Error("no x-hub-signature")
  }
}
